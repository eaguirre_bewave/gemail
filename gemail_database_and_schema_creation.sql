
-- Schema: public

--DROP SCHEMA public;

CREATE SCHEMA public
  AUTHORIZATION postgres;

-- Table: public.outbox

-- DROP TABLE public.outbox;

CREATE TABLE public.outbox
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone NOT NULL,
  deleted_at timestamp with time zone,
  user_json_variable jsonb,
  provider_id character varying(128),
  provider_status character varying(12),
  status character varying(12) NOT NULL,
  provider character varying(7) NOT NULL,
  sender character varying(255),
  recipients character varying[] NOT NULL,
  cc character varying[],
  bcc character varying[],
  subject character varying NOT NULL,
  body text,
  html text,
  attachments text,
  inline_images text,
  reply_to character varying(255),
  retries_counter integer DEFAULT 0,
  severity character varying,
  error character varying,
  CONSTRAINT pk_outbox PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
-- ALTER TABLE public.outbox
--  OWNER TO postgres;

-- New columns

--ALTER TABLE public.outbox
--ADD COLUMN reply_to character varying(255);

--ALTER TABLE public.outbox
--ADD COLUMN severity character varying;

