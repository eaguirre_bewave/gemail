# encoding: utf-8
import sys
import codecs
from setuptools import setup, find_packages


with codecs.open('README.md', encoding='utf-8') as f:
    long_description = f.read()

if 'PyPy' in sys.version:
    PSYCOPG = 'psycopg2cffi'
else:
    PSYCOPG = 'psycopg2-binary'

install_requires = [
    'colander',
    'pip-tools',
    'requests',
    PSYCOPG,
    'spyne',
    'lxml',
    'zeep',
    'consulate',
    'sentry-sdk==0.3.6'
]

dev_requires = [
    # development tools
    'ipython',
]
test_requires = [
    'coverage',
    'flake8',
    'isort',
    'pretend',
    'pytest',
    'tox',
]

extras_require = {
    'test': test_requires,
    'dev': dev_requires + test_requires,
}

setup(
    name='GEmail',
    version='0.0.1',
    description='Garda GEMail Interface',
    long_description=long_description,
    url='https://eaguirre_bewave@bitbucket.org/eaguirre_bewave/gemail.git',
    author='The Garda Team',
    author_email='jean-francois.menard@garda.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Office/Business :: Mailing',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 2.7'
    ],
    keywords='Mailing',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=install_requires,
    extras_require=extras_require,
    entry_points={
        'console_scripts': [
            'sample=sample:main',
        ],
    },
)
