GEmail
Documentation
27 Nov 2018
DESCRIPTION
GEmail is the acronym of Garda Email Web Service which main function is to be a central hub, that gathers and dispatch emails to the corresponding recipients.  As show in the image above, GEmail uses a database table as an outbox container that tracks the contents of the email and the management information of it (like the id, delivery status, mail provider, errors, etc.) 

GEmail is implemented as a Web Service Application and has the ability to send the message thru different email providers (for now we implemented the use of Mailgun) or by the Simple Message Transfer Protocol (SMTP). 

GEmail provides these service functions: 

Send email:  Store the incoming email request to the Outbox table.
Check Status: Queries the delivery status of the email from the Outbox table.
View email: Gets the content information of an email stored in the Outbox table.
Search: Searching emails with a given keyword match the content of the Outbox table fields.

More information about the implementation and use of this commands below.

GEmail works tightly with the GEmail Async email sender which is process that gathers information from the Outbox table and sends the emails to the service providers or directly via SMTP. It also queries the delivery status information of the email from the service provider and updates the Outbox table.

 INSTALLATION

GEmail was designed to use Python 2.7.x version it will require code changes to be used in Python 3.x (we did this to avoid issues with the current installation environment). We recommend that you install a virtual environment in your  machine to avoid conflict version problems with your existing Python installation.
Download GEmail:  You can download GEmail from Bitbucket at:

	TBD

Install PIP:
On Linux type: 	apt install python-pip (for Python 2) 

Install Virtualenv:
On Linux type: 	sudo pip install virtualenv

Create a virtual environment:
	The easiest is to create the virtual environment in the same folder you copied the /GEmail and type:
		virtualenv -p /user/bin/python2.7 venv
	
	 (change the path and Python version to any 2.7.x  that you have installed in your system)
		
Activate the virtual environment by typing:


. ./venv/bin/activate

	   you will see a new prompt like this: 
	  
venv > _

All python installations that you do from now on inside this virtual environment will be contained inside it. To deactivate the virtual environment just type:
	 
venv > deactivate
   
Setup GEmail: Now you just need to setup GEmail to be able to use it. You can do this by typing the next command:

venv > python setup.py install

	This will install all the python libraries that GEmail requires inside your virtual environment.

That is it, now you will need to do some first time configuration for your PostgreSQL and CONSUL before running GEmail (see below).
 
POSTGRESQL CONFIGURATION
Before using GEmail you will need to create a new database schema (gemail_schema) in your PostgreSQL database and create the Outbox Table (gemail_schema.outbox) that will be used to manage the incoming email requests.

To do so you will need to run the next SQL query :

 	-- Schema: gemail_schema
	--DROP SCHEMA gemail_schema;
	CREATE SCHEMA gemail_schema
		AUTHORIZATION postgres;

	-- Table: gemail_schema.outbox
	-- DROP TABLE gemail_schema.outbox;
	CREATE TABLE gemail_schema.outbox
	(
	  id uuid NOT NULL DEFAULT uuid_generate_v4(),
	  created_at timestamp with time zone NOT NULL,
	  updated_at timestamp with time zone NOT NULL,
	  deleted_at timestamp with time zone,
	  user_json_variable jsonb,
	  provider_id character varying(128),
	  provider_status character varying(12),
	  status character varying(12) NOT NULL,
	  provider character varying(7) NOT NULL,
	  recipients character varying[] NOT NULL,
	  cc character varying[],
	  bcc character varying[],
	  subject character varying NOT NULL,
	  body text,
	  html text,
	  attachments text,
	  inline_images text,
  reply_to character varying(255),
	  retries_counter integer DEFAULT 0,
  error character varying,
	  CONSTRAINT pk_outbox PRIMARY KEY (id)
	)
	WITH (
	  OIDS=FALSE
	);
ALTER TABLE gemail_schema.outbox
OWNER TO postgres; 

The outbox table column fields are described as follows:

id:  Unique identifier of the email request (GUID). All programs that connects to GEmail should keep this ID to be able to track the status of the email sent.
created_at: Timestamp of the creation of the new email request record in the outbox table. 
updated_at: Timestamp of the last update of the email request record.
deleted_at: Timestamp of the logical deletion of the email request.
user_json_variable: This is a json field that can hold any user additional information that the external application (that connects to GEmail as a service) needs to manage for each email. The GEmail check status command allows to query the contents of this user variable to make any search that the external application requires. For example  this field can hold the first and last name of the recipient with this json string {'first_name':'Peter', 'last_name':'Parker'} and the check status command can query for example all emails where the last name is Parker (for more information see GEmail Commands below)
provider_id: (optional) this is an optional field depending if we are using a email service provider or not. This field  will contain the unique ID of the email that the provider email service will use to identify this email record internally. This field is transparent to the client external application because GEmail handles this by itself.
provider_status: (optional) this is an optional field depending if we are using a email service provider or not. This field  will contain the delivery status from the service provider.
status: This field will contain the current status of the email request. This status can be query from the check status command. And the possible values are: ready, processing, accepted, delivered, rejected, failed, delivered, waiting or unknown. (for more information see GEmail Commands below)
provider: This is the id of the provider. For now we only handle ‘mailgun’ and ‘smtp’
recipients: This is the list of email addresses of the recipients.
cc: (optional) This is the copy conform list of email addresses.
bcc: (optional) This is the list of the blind carbon copy email addresses (any recipients on the bcc field are not visible to others on the email)
subject: This is the subject title of the email.
body: This is the main text of the email message.
html: (optional) This field represents an html text message. If the contents of this field is present the contents of the body field will be omitted in the message.
attachments:(optional) This field is a text field that will hold a list of attachments of attachments in Base64 format.
inline_images:(optional) This field is a text field that will hold a list of inline images for the html text field in Base64 format.
reply_to:(optional) Default email address where the receiver can reply to the sender
retries_counter: Internal counter of the number of current retries that GEmail has been trying to deliver the email message. If this number is greater than a threshold (defined in async email sender) it will set the status of the email to failed.
error: (optional) This is the full description of the error message (if any) when GEmail tries to send the message and fails.

CONSUL CONFIGURATION
GEmail uses the Consul service to configure itself. Before using GEmail you will need to set the following environment variables: 
If you are using online consul configuration:

GEMAIL_CONFIG_ROOT=<root>  where the root is “gemail”

If using local consul configuration:

  	GEMAIL_CONFIG_ROOT=<root>
GEMAIL_CONSUL_DATA=<json file path>

GEMAIL WITHOUT CONSUL
GEmail can also run without using Consul at all. To enable this we need to create a JSON file that will contain all the attributes needed to run GEmail. Then just set the GEMAIL_CONSUL_DATA environment variable as mentioned before with the command:
venv > export GEMAIL_CONSUL_DATA={Path_to_JSON_file}

Command Example:
export GEMAIL_CONSUL_DATA=~/GEmail/gemail/local_consul_data.json
This local_consul_data.json file should contain the following data:
{
    "global": {
        "config_root": "gemail",
        "gemail": {
	"release":"0.0.1",
            "environment":"dev",
            "sentry": {
                "enable": "False",
                "dns":"https://xxxx@sentry.io/0000000"
	},
	"dev":
	{
		"providers":["mailgun","smtp"],
		"gemail_database_url": "host='localhost' dbname='gsked_ondemand' user='test' password='test'"
	},
	"prod":
	{
		"providers":["mailgun","smtp"],
		"gemail_database_url": ""
	}
       }
    }
}
config_root this is the name of the configuration root key which indicate the name of the key that contains all the information of the GEmail configuration.
release: This is the GEmail current release version.
environment: This is the name of the current environment. For now we show only two environments “dev” and “prod” but the user is freely to create as any number of environments it wants.
sentry: These sentry variables enables and define the connection dns of the tracking error site framework name sentry. For more information on how to set it up please visit https://sentry.io/welcome/
providers: This is the list of the valid email providers. GEmail will verify this list if the user tries to send an email to a provider; if it’s not in the list the request will fail.
gemail_database_url: this is the connection string to the main database where GEmail will be connected to access the Outbox Table.






RUNNING WEB SERVICE
We provide a Python script to show an example on how to run the GEmail Web service for development. Go to the folder gemail/bin, here you will find the script dev_server.py and  just type this command inside the virtual environment:
venv > python ./bin/dev_server.py  
You can use any Wsgi server also. Here, we chose Python's built-in wsgi server but you're not supposed to use it in production.

RUNNING TESTS
Once the GEmail Web Service is running you can validate if it’s working correctly by running a set of tests that you can find inside the folder: /gemail/tests here you will find the script test_service.py . Before running this script just go to the end of the file and change every instance of ‘xxxx@garda.com’ email address with a valid address and uncomment the code:
"""
message_id, timestamp = send_simple_mail(['xxxx@garda.com'],'This is a simple message', 'Welcome to GEmail!')
print(message_id)

message_id, timestamp = send_mail(['xxxx@garda.com', 'yyyy@garda.com],
            'Test message',
            'Greetings!',
            None,
            None,
            '<html><img style="display:block;" src="cid:GardaWorld.svg" width="600" height="64"/></br><h1>Hello</h1><p>This is a html message!</p></html>',
            {'first_name':'Peter', 'last_name':'Parker'},
            None,
            [{'name':'GardaWorld.svg','content':'https://www.garda.com/themes/custom/gw/images/GardaWorld.svg'},
            'mailgun']
        )
print (check_status_from_ids([message_id]))
print (view_email(message_id))

items = search('Greetings',  [{"field":"first_name","operator":"=","value":"Peter","logic_operator":"and"}])
print (items)
"""
and  just type this command inside the virtual environment:
venv > python ./gemail/tests/test_service.py

COMMANDS

As mentioned before GEmail provides these service functions: 

Send email:  Store the incoming email request to the Outbox table and sending the email thru the Async Email Sender. 
This function has the following signature:
send_mail(to, subject, text, cc=None, bcc=None, html=None, user_json_variable=None, attachments=None, inline_images=None, mail_provider = None, replay_to = None)

The valid parameters are:
 to (list): List of destination email.
 subject (str): Subject of email.
 text (string): Text of email.
 cc (list): List visible of copy conform destination.
 bcc (list): List invisible of copy conform destination.
 html (string): Text of email in html format.
 user_json_variable (dict): User variable in json format.
attachments (list): list of name and content of the attachment (the content can be base64 encoding or a URL).
inline_images (list): list of name and content of the inline images (the content can be base64 encoding or a URL).
mail_provider (string): The name of the mail provider; default is 'mailgun'. Possible values defined in config.json. For now only 'mailgun' and 'smtp' are available
reply_to (string): The default email address to reply-to


	The response of this service is a JSON string with the following format:
        		{
            	     "timestamp": "datetime",
	            "success": "boolean",
      		      "data": {
            		    "id": "string",
	                "m	essage": "string"
      		      }
	      }


Check Status: Queries the delivery status of the email from the Outbox table.
This function has the following signature:
check_status(email_ids=None, user_query=None)

The valid parameters are:
 email_ids (list): List of email message id.
 user_query (list): List of user queries with the following format
[{"field":"","operator":"","value":"", "logic_operator":"and"}, {"field":"","operator":"","value":"", logic_operator":"or"}, ...]
 Where:
       + field           is any key from the original json provided in the user json variable
       + operator   should be one of '=', '!='
       + value          any data value 
       + logic_operator  should be one of "and", "or"

The response of this service is a JSON string with the following format:
 	{
            "timestamp": "datetime",
            "success": "boolean",
            "data":
                [
                    {
                        "requested_id":"string",                            The email_id or user_variable_query requested (could be null)
                        "timestamp": "datetime",
                        "success": "boolean",
                        "data": [
                                    {
                                        'id': 'string',                     GEmail Message Id
                                        'provider_id': 'string',            Provider Message Id (could be null)
                                        'user_json_variable': 'string',     GEmail user json variables
                                        'status': 'string',                 GEmail current status
                                        'provider_status': 'string',        Provider delivery status (could be null)
                                        'created_at': 'string',             Timestamp when the email was added to the outbox
                                        'updated_at':'string',              Last updated timestamp
                                        'deleted_at':'string',              Deleted timestamp
                                    }
                                ]
                        "error": "string"
                    }
                ]        
            "error":"string"
        }


View email: Gets the content information of an email stored in the Outbox table.
This function has the following signature:
view_email(email_id=None)

The valid parameter is:
email_id (string): email message id.

The response of this service is a JSON string with the following format:
           
            'timestamp': 'string'                       Current time
            'success': 'boolean',                       True if info is available; False otherwise
            'requested_id': 'string',                   The message id requested
            'data'   :  {
                'id': 'string',                         GEmail Message Id
                'provider_id': 'string',                Provider Message Id (could be null)
                'status': 'string',                     GEmail current status
                'provider_status': 'string',            Provider delivery status (could be null)
                'created_at': 'string',                 Timestamp when the email was added to the outbox
                'updated_at':'string',                  Last updated timestamp
                'deleted_at':'string',                  Deleted timestamp
                'provider': 'string',                   Provider name ('mailgun' or 'smtp')
                'to': 'list',                           Recipients list
                'subject':"string",                     Email subject
                'body':"string",                        Email body text
                'sender':"string",                      Sender "from" email
                'cc':"list",                            CC list
                'bcc':"list",                           BCC list
                'html':"string",                        Html text content
                'user_json_variable':"json",            Json of user variables
                'attachments':"list",                   List of attachments
                'inline_images':"list",                 List of inline_images
                'reply_to':'string'                     Default reply-to email address
            }
            'error': 'string'                           Error if any (optional)
        }
Search: Searching emails with a given keyword match the content of the Outbox table fields (recipients, cc, bcc, subject, body, html, attachments,inline_images and error)

This function has the following signature:
search(keyword, user_query=None)
The valid parameters are:
- email_id (string): email message id.
	- user_query (list): List of user queries with the following format
[{"field":"","operator":"","value":"", "logic_operator":"and"}, {"field":"","operator":"","value":"", logic_operator":"or"}, ...]
 Where:
       + field           is any key from the original json provided in the user json variable
       + operator   should be one of '=', '!='
       + value          any data value 
       + logic_operator  should be one of "and", "or"
The response of this service is a JSON string with the following format:
    
            
                "timestamp": "datetime",
                "success": "boolean",
                "data":
                    {
                        "keyword_matches" :
                        [
                            {
                                "requested_id":"string",      The email_id or user_query requested (could be null)
                                "timestamp": "datetime",
                                "success": "boolean",
                                "data": [
                                         {
                                   'id': 'string',                 GEmail Message Id
                                   'provider_id': 'string',        Provider Message Id (could be null)
                                   'user_json_variable': 'string', GEmail user json variables
                                   'status': 'string',             GEmail current status
                                   'provider_status': 'string',    Provider delivery status (could be null)
                                   'created_at': 'string',         Timestamp when the email was added to the outbox
                                   'updated_at':'string',          Last updated timestamp
                                   'deleted_at':'string',          Deleted timestamp
                                 }],
                                "error": "string"
                            },
                            ...
                        ],
                        "user_query_matches":
                        [
                            {
                                "requested_id":"string",   The email_id or user_query requested (could be null)
                                "timestamp": "datetime",
                                "success": "boolean",
                                "data": [{
                                 'id': 'string',                GEmail Message Id
                                 'provider_id': 'string',       Provider Message Id (could be null)
                                 'user_json_variable': 'string’, GEmail user json variables
                                 'status': 'string',            GEmail current status
                                 'provider_status': 'string',   Provider delivery status (could be null)
                                 'created_at': 'string',        Timestamp when the email was added to the outbox
                                 'updated_at':'string',              Last updated timestamp
                                 'deleted_at':'string',              Deleted timestamp
                                 }],
                                "error": "string"
                            },
                            ...
                        ]
                    }
                "error":"string"
            }
   
