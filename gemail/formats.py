import json

import colander


class ArbitraryMappingType(colander.Mapping):
    """ Mapping type that preserves extra keys. """

    def __init__(self):
        self.unknown = 'preserve'


class StringMappingSchema(colander.MappingSchema):
    """ MappingSchema subclass that can also deserialize from a JSON string.

    This class also converts a None to an empty dict, which is useful for
    optional sequence parameters.

    Note: This should be used to define a sub-schema, not the top-level
    schema class used with `deserialize`.  For example:

        class ItemSchema(StringMappingSchema):
            a = SomeNode()
            b = colander.SchemaNode(SomeType())

        class SomeMethodSchema(colander.MappingSchema):
            item = ItemSchema()

        @deserialize(SomeMethodSchema)
        def some_method(item):
            pass

    The top-level class SomeMethodSchema always works with a real dict
    that's built from JSON or XML input parameters, for example:
    ``{'item': '{"a": 21, "b": 42}'}``.  It's only nested lists/dicts
    that required the use of StringSequenceSchema/StringMappingSchema
    in sub-schema classes, to parse XML parameters sent as strings.
    """

    # XXX rename to StringMappingSubSchema to clarify that this should not
    # be used for top-level schema classes?

    def deserialize(self, cstruct):
        # The schema is missing or None.
        if cstruct in (colander.null, None):
            # But optional.
            if not self.required:
                return {}
            # Invalid if it's defined as mandatory.
            else:
                raise colander.Invalid(self, self.missing_msg)
        elif not isinstance(cstruct, dict):
            try:
                cstruct = json.loads(cstruct)
            except (TypeError, ValueError):
                raise colander.Invalid(self, 'bad data for JSON object: {!r}'.format(cstruct))
        # Only happend if the schema isn't empty and the data are a dict or valid json string.
        return super(StringMappingSchema, self).deserialize(cstruct)


class ArbitraryStringMappingNode(StringMappingSchema):
    # XXX schema_type is an attribute for SchemaNode, not Schema!
    schema_type = ArbitraryMappingType


class StringSequenceSchema(colander.SequenceSchema):
    """ SequenceSchema subclass that can also deserialize a JSON string.

    This class also converts a None to an empty list, which is useful for
    optional sequence parameters.
    """

    def deserialize(self, cstruct):
        if cstruct in (colander.null, None):
            cstruct = []
        elif not isinstance(cstruct, list):
            try:
                cstruct = json.loads(cstruct)
            except (TypeError, ValueError):
                raise colander.Invalid(self, 'bad data for JSON array: {!r}'.format(cstruct))
        return super(StringSequenceSchema, self).deserialize(cstruct)


class StringNode(colander.SchemaNode):
    schema_type = colander.String


class EmailNode(StringNode):
    validator = colander.Email()


class EmailsNode(StringSequenceSchema):
    list_item = EmailNode()


class AttachmentSchema(colander.MappingSchema):
    name = StringNode()
    content = StringNode()


class AttachmentsNode(StringSequenceSchema):
    attachment = AttachmentSchema()

class MsgSendEmailSchema(colander.MappingSchema):
    sender = StringNode()
    to = EmailsNode()
    subject = StringNode()
    text = StringNode()
    cc = EmailsNode(missing=colander.drop)
    bcc = EmailsNode(missing=colander.drop)
    attachments = AttachmentsNode(missing=colander.drop)
    html = StringNode(missing=colander.drop)
    user_variable = ArbitraryStringMappingNode(missing=colander.drop)
    reply_to = StringNode(missing=colander.drop)

class MessageIdsNode(StringSequenceSchema):
    list_item = StringNode()


class UserVariablesNode(StringSequenceSchema):
    list_item = ArbitraryStringMappingNode()


class CheckStatusSchema(colander.MappingSchema):
    message_ids = MessageIdsNode(missing=colander.drop)
    user_variables = UserVariablesNode(missing=colander.drop)

class StatisticsSchema(colander.MappingSchema):
    start_date = StringNode()

class EventQuerySchema(colander.MappingSchema):
    begin = StringNode()
    end = StringNode()
    event = StringNode()
    recipients = EmailsNode(missing=colander.drop)
    url = StringNode(missing=colander.drop)

class EventUrlQuerySchema(colander.MappingSchema):
    url = StringNode()

class ViewEmailSchema(colander.MappingSchema):
    email_id = StringNode()

class SearchEmailSchema(colander.MappingSchema):
    keyword = StringNode()
    user_variables = UserVariablesNode(missing=colander.drop)