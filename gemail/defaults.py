# encoding: utf-8
""" This module contains all default settings.

See ``lib.config`` for information about how to override those
defaults in either Consul or the environment.

"""
from __future__ import absolute_import, division, print_function, unicode_literals

import sys

config_root = 'gemail'

# GEmail defaults
gemail_release = "0.0.1"
gemail_environment = "dev"
gemail_sentry_enable = "False"
gemail_sentry_dns = "https://xxxx@sentry.io/0000000"

# Development environment variables
gemail_dev_providers = ["mailgun","smtp"]
#gemail_dev_database_url = "host='localhost' dbname='gsked_ondemand' user='test' password='test'"
gemail_dev_database_url = 'postgresql://test:test@localhost/gsked_ondemand'
gemail_dev_connect_timeout= "15"
gemail_dev_force_provider = {
					            "garda": {
						            "enable":"False",
						            "provider":"smtp",
						            "address": "\\W*(@garda\\.(com|ca))\\W"
					            }
				            }
# Production environment variables
gemail_prod_providers = ["mailgun","smtp"]
gemail_prod_database_url = ""
gemail_prod_connect_timeout= "15"
gemail_prod_force_provider = {
					            "garda": {
						            "enable":"False",
						            "provider":"smtp",
						            "address": "\\W*(@garda\\.(com|ca))\\W"
            					}       
				             }
			