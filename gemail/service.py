import functools
import inspect
import json
import logging.config
import re
import sys
from datetime import datetime
from .lib import config
from . import defaults
import colander

from .error import BadRequest, BaseError, InternalServerError

logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s %(levelname)-8s %(name)-15s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': logging.WARNING,
            'stream': sys.stdout,
        },
    },
    'loggers': {
        'zeep.transports': {
            'handlers': ['console'],
            'level': logging.WARNING,
            'propagate': True,
        },
        'gemail-service': {
            'handlers': ['console'],
            'level': logging.WARNING,
            'propagate': True,
        },
    },
})


logger = logging.getLogger('gemail-service')

# Enable Sentry if needed
enableSentry = None
try:
    enableSentry = config.get('%s/sentry/enable' % defaults.config_root)
except:
    logger.error("gmail() Sentry enable variable not found in consul")
if enableSentry is not None and enableSentry == "True":
    dnsSentry = None
    releaseSentry = None
    environmentSentry = None
    try:
        dnsSentry = config.get('%s/sentry/dns' % defaults.config_root)
    except:
        logger.error("gmail() Could not found the Sentry DNS key in consul")
    
    try:
        releaseSentry = config.get('%s/release' % defaults.config_root)
    except:
        logger.error("gmail() Could not found the release key in consul")
    
    try:
        environmentSentry = config.get('%s/environment' % defaults.config_root)
    except:
        logger.error("gmail() Could not found the environment key in consul")

    if dnsSentry and releaseSentry and environmentSentry:
        # Set Sentry Integration
        sentry_logging = LoggingIntegration(
            level=logging.WARNING, # Capture info and above as breadcrumbs
            event_level=None       # Send no events from log messages
        )   

        #Initialize Sentry
        sentry_sdk.init(
            dsn=dnsSentry,
            integrations=[sentry_logging],
            release = releaseSentry,
            environment = environmentSentry
        )
        logger.info("Sentry Initialized!")


def get_argspec_from_schema(schema):
    """ Create an ArgSpec from a colander schema instance."""
    if schema:
        required = [node.name for node in schema.children if node.required]
        optional = [node.name for node in schema.children if not node.required]
        defaults = tuple(None for _ in optional) if optional else None
        args = required + optional
    else:
        args = None
        defaults = None
    return inspect.ArgSpec(args=args, varargs=None, keywords=None, defaults=defaults)


def prepare_method(module, func_name, schema_class):
    schema = schema_class() if schema_class else None

    # Helper function that defines metadata for a service method.
    def decorator(func):
        func.__module__ = module
        func.__name__ = func_name

        # Redefine the function parameters from the schema.
        argspec = get_argspec_from_schema(schema)
        func.function_argspec = argspec

        return func

    return decorator


def expose(schema_class):
    """Factory to create the expose decorator.

    Args:
        schema_class (class): colander Schema subclass.

    Returns: A decorator that will use the schema to deserialize
             the positional arguments given to the function.

    Examples:

        @expose(LoginSchema)
        def login(self, username, password):
            pass
    """
    schema = schema_class() if schema_class else None

    def decorator(func):
        argspec = getattr(func, 'function_argspec', inspect.getargspec(func))
        argnames = tuple(argspec.args or [])

        @functools.wraps(func)
        def expose_wrapper(*args, **kwargs):
            allargs = dict(zip(argnames, args))
            allargs.update(kwargs)

            error_response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'data': None,
            }

            # Validate arguments.
            try:
                schema.deserialize(allargs)
            except colander.Invalid as exc:
                e = BadRequest(exc.asdict())
                error_response['error'] = e.serialize()
                return json.dumps(error_response)
            except AttributeError:
                pass

            try:
                return func(*args, **kwargs)

            except BaseError as exc:
                # These exceptions are always raised by ouer code to be sent
                # directly to the client. If they contain extra info (typically
                # internal data), they are logged, to allow for later inspection
                # and debugging, otherwise they're not saved by default.
                if exc.extra:
                    # log the original exception
                    err = '{}: {}'.format(func.__name__, exc.extra)
                    logger.exception(err)
                error_response['error'] = exc.serialize()
                return json.dumps(error_response)

            except Exception as exc:
                # log the original exception
                err = '{}: {}'.format(func.__name__, str(exc))
                logger.exception(err)
                error_response['error'] = InternalServerError(str(exc)).serialize()
                return json.dumps(error_response)

        expose_wrapper.service_method = True
        expose_wrapper.function_argspec = argspec
        expose_wrapper.schema = schema

        return expose_wrapper

    return decorator


class Service(object):
    service_name = None

    @classmethod
    def update_arguments(cls, arguments):
        """Update arguments right before invoke the method.

        Note: The default implementation just returns the
        received arguments without any modification.
        """
        return arguments

    @classmethod
    def expose_method(cls, module, func_name, schema_class):
        @expose(schema_class)
        @prepare_method(module, func_name, schema_class)
        def passthrough_wrapper(*args, **kwargs):
            argspec = passthrough_wrapper.function_argspec
            if argspec.args:
                arg_names = tuple(argspec.args)
                arguments = dict(zip(arg_names, args))
                arguments.update(kwargs)
            else:
                arguments = kwargs

            arguments = cls.update_arguments(arguments)
            response = cls.invoke(func_name, **arguments)

            return json.dumps(response)

        if hasattr(module, passthrough_wrapper.__name__):
            raise InternalServerError('The method `%s` already exists in module `%s`' % (
                passthrough_wrapper.__name__, module.__name__))

        setattr(module, passthrough_wrapper.__name__, passthrough_wrapper)

    @staticmethod
    def _parse_response(content, action):
        """Parsing the given xml returning the embedded json content.

        Assuming a response with the following format:

        <?xml version='1.0' encoding='UTF-8'?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Body>
            <login_resp>
              <item>
                <response>
                    *** JSON Content ***
                </response>
              </item>
            </login_resp>
          </soap:Body>
        </soap:Envelope>

        Where the json content has the following format:

            Success:
            {
                "data": a json object -> {} or [],
                "success": true,
                "timestamp": "2018-02-14T20:47:08Z"
            }

            Failure:
            {
                "data": null,
                "error": {
                    "code": 400,
                    "label": "Bad Request",
                    "message": "{\"password\": \"Required\"}"
                },
                "success": false,
                "timestamp": "2018-02-14T20:47:08Z"
            }

        Returns:
            dict:
            {
                "data": {} or [],
                "error": {},
                "success": True or False,
                "timestamp": "2018-02-14T20:47:08Z"
            }
        """
        pattern = ('<{action}_resp>.*<item>.*<response>'
                   '(?P<json_content>.*)'
                   '</response>.*</item>.*</{action}_resp>'.format(action=action))
        regex = re.compile(pattern, re.DOTALL)
        return regex.findall(content)
