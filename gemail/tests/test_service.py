#!/usr/bin/python
# encoding: utf-8
from __future__ import absolute_import, division, print_function, unicode_literals

import zeep
import logging
import json
from datetime import datetime
from datetime import timedelta

logger = logging.getLogger('test_service')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

def dump_json_or_none(value, isOptional = False):
    if isOptional and value is None:
        return None
    return json.dumps(value)

def send_simple_mail(sender,recipients,subject,body):
    """
        Example:

        message_id, timestamp = send_simple_mail('The wizard <xxxx@garda.com>',['xxxxx@garda.com'],'Test message', 'Greetings!','xxxxx@garda.com')
        logger.debug(message_id)

    """
    message_id = None
    timestamp = None

    #convert recipients to json
    recipients = dump_json_or_none(recipients)
 
    try:
        client = zeep.Client('http://localhost:8001?WSDL')
        result = client.service.send_mail(sender,
                        recipients,
                        subject,
                        body,
                        None,None,None,None,None,None,
                        'mailgun',None)
        if result:
            data = json.loads(result)
            if data['success']:
                message_id = data['data']['id']
                timestamp = data['timestamp']
            else:
                logger.error('Error %s %s : %s' % (data['error']['code'],
                                data['error']['label'],
                                data['error']['message']))
        else:
            logger.error("result is null")
    except Exception as e:
        logger.error("Exception occurred! %s" % e)
    return message_id, timestamp

def check_status_from_ids(ids, user_variable = None):
    """
        Example:

        items, timestamp = check_status_from_ids([message_id], None)
        logger.debug(items)
    """

    items = None
    
    ids = dump_json_or_none(ids)
    user_variable = dump_json_or_none(user_variable, True)

    try:
        client = zeep.Client('http://localhost:8001?WSDL')
        result = client.service.check_status(ids, user_variable)
        if result:
            data = json.loads(result)
            if data['success']:
                items = data['data']
            else:
                logger.error('Error %s %s : %s' % (data['error']['code'],
                                data['error']['label'],
                                data['error']['message']))
        else:
            logger.error("result is null")
    except Exception as e:
        logger.error("Exception occurred! %s" % e)
    return items

def view_email(id):
    """
        Example:

        item, timestamp = view_email(message_id)
        logger.debug(item)
    """
    item = None
    timestamp = None
    try:
        client = zeep.Client('http://localhost:8001?WSDL')
        result = client.service.view_email(id)
        if result:
            data = json.loads(result)
            if data['success']:
                timestamp = data['timestamp']
                item = data['data']
            else:
                logger.error('Error %s %s : %s' % (data['error']['code'],
                                data['error']['label'],
                                data['error']['message']))
        else:
            logger.error("result is null")
    except Exception as e:
        logger.error("Exception occurred! %s" % e)
    return item, timestamp

def send_mail(sender,recipients, subject, body, cc=None, bcc=None, html=None,user_variable=None,
                 attachments=None, inline_images=None, mail_provider='mailgun', reply_to=None):
    message_id = None
    timestamp = None

    #convert recipients to json
    recipients = dump_json_or_none(recipients)
    cc =  dump_json_or_none(cc, True)
    bcc =  dump_json_or_none(bcc, True)
    user_variable =  dump_json_or_none(user_variable, True)
    attachments = dump_json_or_none(attachments, True)
    inline_images = dump_json_or_none(inline_images, True)

    try:
        client = zeep.Client('http://localhost:8001?WSDL')
        result = client.service.send_mail(sender,
                        recipients,
                        subject,
                        body,
                        cc,
                        bcc,
                        html,
                        user_variable,
                        attachments,
                        inline_images,
                        mail_provider,
                        reply_to)
        if result:
            data = json.loads(result)
            if data['success']:
                message_id = data['data']['id']
                timestamp = data['timestamp']
            else:
                logger.error('Error %s %s : %s' % (data['error']['code'],
                                data['error']['label'],
                                data['error']['message']))
        else:
            logger.error("result is null")
    except Exception as e:
        logger.error("Exception occurred! %s" % e)
    return message_id, timestamp

def search(keyword, user_variable = None):
    """
        Example:

        items, timestamp = search(keyword, None)
        logger.debug(items)
    """

    items = None
    
    user_variable = dump_json_or_none(user_variable, True)
    try:
        client = zeep.Client('http://localhost:8001?WSDL')
        result = client.service.search(keyword, user_variable)
        if result:
            data = json.loads(result)
            if data['success']:
                items = data['data']
            else:
                logger.error('Error %s %s : %s' % (data['error']['code'],
                                data['error']['label'],
                                data['error']['message']))
        else:
            logger.error("result is null")
    except Exception as e:
        logger.error("Exception occurred! %s" % e)
    return items


#message_id, timestamp = send_simple_mail('The wizard of OZ <from_email@garda.com>',['emilio.aguirre@bewave.io'],'Another simple message', 'Welcome to GEmail!')
#print(message_id)

import base64
encoded_string = ''
with open("./gemail/tests/9000004303.pdf", "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())

message_id, timestamp = send_mail(
            'The wizard of OZ <from_email@garda.com>',
            ['emilio.aguirre@bewave.io'],
            'Test message',
            'Greetings!',
            None,
            None,
            u'<html>' \
            u'<p>Dear Customer, <br>' \
            u'<br>' \
            u'Please see the attached document to this email for a copy of your invoice: {{invoice.invoice_number}}.<br>' \
            u'<br>' \
            u'Should you have any questions or concerns please call 1-800-859-1599 ext: 2188 or e-mail: arps@garda.com <br>' \
            u'<br>' \
            u'Thank you, <br>'\
            u'<br>'\
            u'Garda World - Accounts receivable Team </p><br>'\
            u'<br>' \
            u'_____________________________________________________________________________________ <br>'\
            u'<br>' \
            u'<p>Cher Client, <br>' \
            u'<br>' \
            u'Vous trouverez en pièce jointe à ce courriel une copie de votre facture: {{invoice.invoice_number}}.<br>'\
            u'<br>' \
            u'Pour vos questions ou commentaires, veuillez nous contacter au : 1-800-859-1599 ext: 2188 ou par courriel : arps@garda.com <br>'\
            u'<br>'\
            u'Merci beaucoup, <br>'\
            u'<br>'\
            u'L\'équipe des comptes recevables - GardaWorld</p>' \
            u'</html>',
            {'first_name':'Peter', 'last_name':'Parker'},
            [{'name':'9000004303.pdf','content':encoded_string}],
            None,
            'mailgun',
            'The reply email <reply_to@garda.com>'
        )
print (message_id)
print (check_status_from_ids([message_id]))
#print (view_email(message_id))

#items = search('Greetings',  [{"field":"first_name","operator":"=","value":"Peter","logic_operator":"and"}])
#print (items)"""