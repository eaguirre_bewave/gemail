class BaseError(Exception):
    """ Base class for GEmail Service exceptions.

    Attributes that should be overriden by subclasses:
        code (int): error code (taken from the HTTP spec)
        label (string): short description of the error
        message (string): free-form description of the error.

    The __init__ method accepts a "message" argument, to use an error
    message that's more specific than the default class attribute
    "message", and an "extra" argument with the full original error
    information.

    Subclasses can override __init__, but they should call the parent
    class' __init__.  They may also override serialize.
    """
    code = None
    label = None
    message = None

    def __init__(self, message=None, extra=None):
        self.message = message or self.message or self.label
        self.extra = extra
        super(BaseError, self).__init__(self.message)

    def serialize(self):
        return {'code': self.code,
                'label': self.label,
                'message': self.message}


class BadRequest(BaseError):
    code = 400
    label = 'Bad Request'
    message = 'Parameters malformed.'


class Unauthorized(BadRequest):
    code = 401
    label = 'Unauthorized'
    message = 'Missing or invalid authentication.'


class Forbidden(BadRequest):
    code = 403
    label = 'Forbidden'
    message = 'Access denied.'


class NotFound(BadRequest):
    code = 404
    label = 'Not Found'
    message = 'Data not found.'


class Conflict(BadRequest):
    code = 409
    label = 'Conflict'
    message = 'Conflict with the current state of the resource.'


class InternalServerError(BaseError):
    code = 500
    label = 'Internal Server Error'
    message = 'Server encountered an unexpected error.'
