import inspect
from collections import OrderedDict

from spyne import ServiceBase, srpc
from spyne.model import primitive
from spyne.model.primitive import Unicode

from . import gemail

# All types are exposed as String in the gemail service wsdl to avoid extra
# deserialization / serialization.
# If we want expose the true type in the future, we need to create a mapping like this one:
# _param_type_map = {
#     colander.Date: 'Date',
#     colander.DateTime: 'DateTime',
#     colander.Decimal: 'Decimal',
#     colander.Int: 'Integer',
#     colander.Time: 'Time',
# }
# But that will transform the text arguments to python objects, ex: a date `2018-01-04` will be
# parsed and transformed to datetime.date(2018, 1, 4). In this case we should serialize the
# arguments before to call the original service.

def get_schema_nodes(schema, params):
    if not schema:
        return dict.fromkeys(params)

    return {node.name: node
            for node in schema.children
            if node.name in params}


def get_param_type(param, schema_nodes, is_required):
    # XXX: Using `Unicode` always instead of `_param_type_map.get(schema_type, 'Unicode')`
    # where the schema_type should be fetched in this way `type(schema_nodes[param].typ)`.
    spyne_type_name = 'Unicode'

    if is_required:
        base = primitive.Mandatory
    else:
        base = primitive

    return getattr(base, spyne_type_name)


def get_params_types(required, optional, schema):
    result = []

    schema_nodes = get_schema_nodes(schema, required)
    for param in required:
        spyne_type = get_param_type(param, schema_nodes, True)
        result.append(spyne_type)

    schema_nodes = get_schema_nodes(schema, optional)
    for param in optional:
        spyne_type = get_param_type(param, schema_nodes, False)
        result.append(spyne_type)

    return result


def get_method_inputs(func):
    argspec = getattr(func, 'function_argspec', inspect.getargspec(func))
    args = argspec.args or []
    if argspec.defaults:
        number_optional = len(argspec.defaults)
        required_params = args[:-number_optional]
        optional_params = args[-number_optional:]
    else:
        required_params = args[:]
        optional_params = []

    return required_params, optional_params


def format_docstring(doc):
    doc = doc or 'No documentation.'

    # Remove leading space and convert Python terms to service terms.
    doc = doc.strip()
    replacements = (
        (' Args:', ' Request parameters:'),
        (' Returns:', ' Response data payload:'),
        (' Raises:', ' Errors:'),
    )
    for source, replacement in replacements:
        doc = doc.replace(source, replacement)

    lines = doc.splitlines()
    first, lines = lines[0], lines[1:]
    lines = [('    ' + line if line else line) for line in lines]
    lines.insert(0, first)
    return '\n'.join(lines)


def generate_spyne_method(func):
    required, optional = get_method_inputs(func)

    args = required + optional

    # Convert params/schema nodes to Spyne types
    schema = getattr(func, 'schema', None)
    params_types = get_params_types(required, optional, schema)

    # Strip extra spaces at beginning and end to avoid extra spaces in WSDL
    func.__doc__ = format_docstring(func.__doc__)

    # Now put everything together
    deco = srpc(*params_types, _args=args, _returns=Unicode)

    spyne_method = deco(func)
    spyne_method.__name__ = func.__name__
    return spyne_method


def _on_method_return_object(ctx):
    ctx.transport.resp_headers.update({
        'Content-Type': 'application/soap+xml; charset=utf-8',
        'Access-Control-Allow-Origin': '*',
    })


def generate_spyne_class():
    doc = """mail gun interface application service.

    Note: This is not a method, just a placeholder for
          general documentation in the WSDL file.

    Data types for request parameters:

        string: UTF-8 encoded string
        date: string with ISO/W3C format 2015-01-30
        time: string, format 12:30 or 12:30:42
        datetime: string, format 2015-01-30T12:00:00Z, with timezone
        local datetime: string, format 2015-01-30T12:00:00, without timezone
        list: string, JSON array format [value, ...]
        dict: string, JSON object format {"key": value, ...}
        none: JSON null, or omit parameter

    Standard payload for all responses:

        {
          "timestamp": "datetime",
          "success": true,
          "data": <response data payload>
        }


    If an error happened:

        {
          "timestamp": "date/time",
          "success": false,
          "error": {
            "code": number,
            "label": "string",
            "message": "string that can be displayed to user"
          },
          "data": <null or payload>
        }

    Error codes and labels:

        400 "Bad Request": generic client error
        401 "Unauthorized": invalid credentials or missing token
        403 "Forbidden": token not allowed for requested app or branch
        404 "Not Found": resource or data not found, deleted or expired
        409 "Conflict": impossible to accept create/update
        500 "Internal Server Error": unexpected server error
    """
    class_attrs = {'name': 'gemail-service'}
    class_name = 'gemailService'
    sorted_methods = []

    def info():
        pass

    info.__doc__ = doc
    info = srpc()(info)
    class_attrs['info'] = info
    sorted_methods.append('info')

    gemail_methods = sorted((obj for obj in gemail.__dict__.values()
                              if hasattr(obj, 'service_method')), key=lambda obj: obj.__name__)
    for func in gemail_methods:
        sorted_methods.append(func.__name__)
        spyne_method = generate_spyne_method(func)
        class_attrs[spyne_method.__name__] = spyne_method

    cls = type(class_name, (ServiceBase,), class_attrs)

    # Override self.public_methods with an ordered dict to keep the order we want
    # when Spyne iterates over public methods to create the WSDL file.
    cls.public_methods = OrderedDict(
        (name, cls.public_methods[name]) for name in sorted_methods)

    cls.event_manager.add_listener('method_return_object', _on_method_return_object)

    return cls
