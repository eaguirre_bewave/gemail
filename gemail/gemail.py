import base64
import binascii
import json
import os
import psycopg2
import re

from psycopg2.extras import wait_select
from datetime import datetime
from datetime import timedelta
from operator import itemgetter

from requests import get, post
from .lib import config

from . import formats
from .error import BadRequest, NotFound, InternalServerError
from .service import expose

MAILGUN_PROVIDER = 'mailgun'
SMTP_PROVIDER = 'smtp'
   
# ----------------------------------------------
# Exposed Web service functions 
# ----------------------------------------------
@expose(formats.MsgSendEmailSchema)
def send_mail(sender, to, subject, text, cc=None, bcc=None, html=None, user_json_variable=None, attachments=None, 
                inline_images=None, mail_provider = None, reply_to = None):
    """Send email to service.

    Args:
        sender (string):  Sender "From" email. 
        to (list): List of destination email.
        subject (string): Subject of email.
        text (string): Text of email.
        cc (list): List visible of copy conform destination.
        bcc (list): List invisible of copy conform destination.
        html (string): Text of email in html format.
        user_json_variable (dict): User variable in json format.
        attachments (list): list of name and content of the attachment (the content can be base64 encoding or a URL).
        inline_images (list): list of name and content of the inline images (the content can be base64 encoding or a URL).
        mail_provider (string): The name of the mail provider; default is 'mailgun'. Possible values defined in config.json. 
                                 For now only 'mailgun' and 'smtp' are available
        reply_to (string): Default reply to email. Can be of this format "Our great company <info@garda.com>"
    Returns:
        {
            "timestamp": "datetime",
            "success": "boolean",
            "data": {
                "id": "string",
                "message": "string"
            }
        }
    """
    
    # Select environment (production or development)
    environment = config.get('gemail/environment')
    
    # Check if there is a force provider in 'to' field
    try:
        force_provider = config.get('gemail/%s/force_provider' % environment)
    except:
        force_provider = None
    if force_provider:
        for _,value in force_provider.items():
            if "enable" in value and "provider" in value and "address" in value:
                if value['enable'] == "True":
                    pattern = value['address']
					# match ignoring case 
                    matchObj = re.search(pattern,to,re.I)
                    if matchObj:
                        # At least one of the email address of the 'to' field
                        # match the force provider, so let's assign that provider
                        # and exit the loop
                        mail_provider = value['provider']
                        break
                        
    # Validate service provider    
    providers = config.get('gemail/%s/providers' % environment)
    if mail_provider not in providers:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': NotFound(message="Invalid mail provider. Valid values are :%s " % providers).serialize()
        }
        return json.dumps(response)

    if user_json_variable:
        user_var = json.loads(user_json_variable)
        if isinstance(user_var, dict) == False:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'data': None,
                'error': BadRequest(message="Invalid user_json_variable (%s). This value should be a dictionary. " % user_json_variable).serialize()
                }
            return json.dumps(response)
    
    attachments = attachments if attachments else []
    inline_images = inline_images if inline_images else []
    
    # convert string lists to objects if needed
    to = _str_to_obj(to)
    cc = _str_to_obj(cc)
    bcc = _str_to_obj(bcc)
    attachments = _str_to_obj(attachments)
    inline_images = _str_to_obj(inline_images)

    # convert base64 attachments to bytes
    (error, attach) = _get_attachment(attachments)
    if error:
        return error
    attachments = json.dumps(attach)  

    # convert base64 inline images to bytes
    (error, images) = _get_inline_images(inline_images)
    if error:
        return error
    inline_images = json.dumps(images)          
     
    gemail_response = _outbox_add(sender,
                                    mail_provider,
                                    to,
                                    subject,
                                    text,
                                    cc,
                                    bcc,
                                    html,
                                    user_json_variable,
                                    attachments,
                                    inline_images,
                                    reply_to)
    if gemail_response:
        json_obj = json.loads(gemail_response)
        if json_obj.get('id'):
            response = {
                'timestamp': str(datetime.now()),
                'success': True,
                'data': json_obj
            }
        else:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'data': None,
                'error':json_obj.get('error')
            }
    else:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': NotFound(message="Error we got a null response").serialize()
        }
    return json.dumps(response)

@expose(formats.CheckStatusSchema)
def check_status(email_ids=None, user_query=None):
    """Get email status.

    Args:
        email_ids (list): List of email message id.
        user_query (list): List of user queries.
        
        The format expected in the user variable query is:

        [
            {"field":"","operator":"","value":"", "logic_operator":"and"},
            {"field":"","operator":"","value":"", logic_operator":"or"}, 
             ...
        ]
    
        Where:
            field           is any key from the original json provided in the user json variable
            operator        should be one of '=', '!='
            value           any data value 
            logic_operator  "and", "or"

    Returns:
        {
            "timestamp": "datetime",
            "success": "boolean",
            "data":
                [
                    {
                        "requested_id":"string",                            The email_id or user_query requested (could be null)
                        "timestamp": "datetime",
                        "success": "boolean",
                        "data": [
                                    {
                                        'id': 'string',                     GEmail Message Id
                                        'provider_id': 'string',            Provider Message Id (could be null)
                                        'user_json_variable': 'string',     GEmail user json variables
                                        'status': 'string',                 GEmail current status
                                        'severity': 'string',               GEmail severity. The email provider can set the status to 
                                                                            failed o rejected permanent or temporary (could be null)
                                        'provider_status': 'string',        Provider delivery status (could be null)
                                        'error': 'string',                  Provider error message (could be null)                                                         
                                        'created_at': 'string',             Timestamp when the email was added to the outbox
                                        'updated_at':'string',              Last updated timestamp
                                        'deleted_at':'string',              Deleted timestamp
                                    }
                                ]
                        "error": "string"
                    }
                ]        
            "error":"string"
        }
    """
    email_ids = [] if not email_ids else email_ids
    user_query = [] if not user_query else user_query

    email_ids = _str_to_obj(email_ids)
    user_query = _str_to_obj(user_query)
    
    if not email_ids and not user_query:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': BadRequest(
                message='Check status. You must specified email_ids or user_query').serialize()
        }
        return json.dumps(response)

    result=[]
    if email_ids and len(email_ids) > 0:
        for message_id in email_ids:
            db_result = _outbox_get_status(message_id)
            if (db_result):
                result.append(json.loads(db_result))

    if user_query and len(user_query) > 0:
        db_result = _outbox_get_user_data_status(user_query)
        if (db_result):
            result.append(json.loads(db_result))

    result = sorted(result, key=itemgetter('timestamp'))
    if len(result)>0:
        response = {
            'timestamp': str(datetime.now()),
            'success': True,
            'data': result
        }
    else:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': NotFound(message="Check status. Error retrieving data").serialize()
        }
    return json.dumps(response)

@expose(formats.ViewEmailSchema)
def view_email(email_id=None):
    """View email will get all the content of the given email id.

    Args:
        email_id (string): email message id.

    Returns:
            {
                "timestamp": "datetime",
                "success": "boolean",
                "data":
                    {
                        'timestamp': 'string'                       Current time
                        'success': 'boolean',                       True if info is available; False otherwise
                        'requested_id': 'string',                   The message id requested
                        'data'   :  {
                            'id': 'string',                         GEmail Message Id
                            'provider_id': 'string',                Provider Message Id (could be null)
                            'status': 'string',                     GEmail current status
                            'provider_status': 'string',            Provider delivery status (could be null)
                            'created_at':'string',                  Timestamp when the email was added to the outbox
                            'updated_at':'string',                  Last updated timestamp
                            'deleted_at':'string',                  Deleted timestamp
                            'provider': 'string',                   Provider name ('mailgun' or 'smtp')
                            'sender':"string",                      Sender "From"
                            'to': 'list',                           Recipients list
                            'subject':"string",                     Email subject
                            'body':"string",                        Email body text
                            'cc':"list",                            CC list
                            'bcc':"list",                           BCC list
                            'html':"string",                        Html text content
                            'user_json_variable':"json",            JSON string with user variables
                            'attachments':"list",                   List of attachments
                            'inline_images':"list",                 List of inline_images
                            'reply_to':'string'                     Default reply-to email address
                        }
                    }    
                "error": 'string'
            }
    """
    if not email_id:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': BadRequest(
                message='you must specified email_id').serialize()
        }
        return json.dumps(response)

    response = _outbox_get_email(email_id)
    if response:
        json_obj = json.loads(response) 
        success = json_obj.get('success')
        if success:
            response = {
                'timestamp': str(datetime.now()),
                'success': True,
                'data': json_obj
            }
        else:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'data': None,
                'error': json_obj.get('error')
            }
    else:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': NotFound(message="Error we got a null response").serialize()
        }
    return json.dumps(response)

@expose(formats.SearchEmailSchema)
def search(keyword, user_query=None):
    """Search emails that contains the given keyword inside recipients, cc, bcc, subject, body, html, attachments,
        and inline_images fields. Or emails that match the user variables with the given user query . 

    Args:
        email_id (string): email message id.
        user_query (list): List of user queries.
        
        The format expected in the user variable query is:

        [
            {"field":"","operator":"","value":"", "logic_operator":"and"},
            {"field":"","operator":"","value":"", logic_operator":"or"}, 
             ...
        ]
    
        Where:
            field           is any key from the original json provided in the user json variable
            operator        should be one of '=', '!='
            value           any data value 
            logic_operator  "and", "or"

    Returns:
    
            {
                "timestamp": "datetime",
                "success": "boolean",
                "data":
                    {
                        "keyword_matches" :
                        [
                            {
                                "requested_id":"string",                            The email_id or user_query requested (could be null)
                                "timestamp": "datetime",
                                "success": "boolean",
                                "data": [
                                            {
                                                'id': 'string',                     GEmail Message Id
                                                'provider_id': 'string',            Provider Message Id (could be null)
                                                'user_json_variable': 'string',     GEmail user json variables
                                                'status': 'string',                 GEmail current status
                                                'severity': 'string',               GEmail severity. The email provider can set the status to 
                                                                            failed o rejected permanent or temporary (could be null)
                                                'provider_status': 'string',        Provider delivery status (could be null)
                                                'error': 'string',                  Provider error message (could be null)                                                         
                                                'created_at': 'string',             Timestamp when the email was added to the outbox
                                                'updated_at':'string',              Last updated timestamp
                                                'deleted_at':'string',              Deleted timestamp
                                            }
                                        ]
                                "error": "string"
                            },
                            ...
                        ],
                        "user_query_matches":
                        [
                            {
                                "requested_id":"string",                            The email_id or user_query requested (could be null)
                                "timestamp": "datetime",
                                "success": "boolean",
                                "data": [
                                            {
                                                'id': 'string',                     GEmail Message Id
                                                'provider_id': 'string',            Provider Message Id (could be null)
                                                'user_json_variable': 'string',     GEmail user json variables
                                                'status': 'string',                 GEmail current status
                                                'severity': 'string',               GEmail severity. The email provider can set the status to 
                                                                                      failed o rejected permanent or temporary (could be null)
                                                'provider_status': 'string',        Provider delivery status (could be null)
                                                'error': 'string',                  Provider error message (could be null)                                                         
                                                'created_at': 'string',             Timestamp when the email was added to the outbox
                                                'updated_at':'string',              Last updated timestamp
                                                'deleted_at':'string',              Deleted timestamp
                                            }
                                        ]
                                "error": "string"
                            },
                            ...
                        ]
                    }
                "error":"string"
            }
    """
    if not keyword:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': BadRequest(
                message='you must specified the keyword').serialize()
        }
        return json.dumps(response)

    user_query = [] if not user_query else user_query
    user_query = _str_to_obj(user_query)

    result={'keyword_matches':[], 'user_query_matches':[]}
    db_result = _search_emails(keyword)
    if (db_result):
        result['keyword_matches'].append(json.loads(db_result))
        result['keyword_matches'] = sorted(result['keyword_matches'], key=itemgetter('timestamp'))

    if user_query and len(user_query) > 0:
        db_result = _outbox_get_user_data_status(user_query)
        if (db_result):
            result['user_query_matches'].append(json.loads(db_result))
            result['user_query_matches'] = sorted(result['user_query_matches'], key=itemgetter('timestamp'))
    
    if len(result['keyword_matches'])>0  or len(result['user_query_matches'])>0:
        response = {
            'timestamp': str(datetime.now()),
            'success': True,
            'data': result
        }
    else:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'data': None,
            'error': NotFound(message="Search command. Error retrieving data").serialize()
        }
    return json.dumps(response)
  
# ----------------------------------------------
# Helper private functions 
# ----------------------------------------------
def _str_to_obj(data):
    obj = data
    if isinstance(data, basestring):
        try:
            obj = json.loads(data)
        except:
            obj = []
    return obj

def _get_attachment(attachments):
    return _encode_base64("attachment", attachments)

def _get_inline_images(images):
    for img in images:
        if isinstance(img, dict):
            name = img['name']
            _,ext = os.path.splitext(name)
            if ext.lower() not in ['.jpeg', '.jpg', '.png', '.gif', '.svg', '.bmp']:
                error = {
                    'timestamp': str(datetime.now()),
                    'success': False,
                    'data': None,
                    'error': BadRequest(
                        message='inline image %s has invalid extension! Should be one of: jpeg, jpg, png, gif, svg, bmp' % name).serialize()
                }
                return json.dumps(error), []
    return _encode_base64("inline", images)

def _encode_base64(item_type, items):
    error = {}
    files = []
    for item in items:
        if isinstance(item, dict):
            file_content = item['content']
            file_name = item['name']

            # Check for URL's
            if file_content.startswith('https://') or file_content.startswith('http://'):
                try:
                    response = get(file_content)
                    file_content = response.content
                    try:
                        file_content = base64.b64encode(file_content)
                    except Exception as err1:
                        response = {
                            'timestamp': str(datetime.now()),
                            'success': False,
                            'data': None,
                            'error': BadRequest(
                                message='failed to encode base64 data: {}'.format(err1)).serialize()
                        }
                        return json.dumps(response), files

                except Exception as err2:
                    error = {
                        'timestamp': str(datetime.now()),
                        'success': False,
                        'data': None,
                        'error': BadRequest(
                            message='failed downloading URL content for %s : %s' % (item_type, err2)).serialize()
                    }
                    return json.dumps(error), files
            # Check for SVG files
            elif file_content.startswith('<?xml'):
                try:
                    file_content = base64.b64encode(file_content)
                except Exception as err1:
                    response = {
                        'timestamp': str(datetime.now()),
                        'success': False,
                        'data': None,
                        'error': BadRequest(
                             message='failed to encode base64 data: {}'.format(err1)).serialize()
                    }
                    return json.dumps(response), files
            else:
                # Let's validate that the given data is a valid Base64
                try:
                    file_content = file_content.encode('ascii')
                except Exception as err:
                    error = {
                        'timestamp': str(datetime.now()),
                        'success': False,
                        'data': None,
                        'error': BadRequest(
                            message='failed to convert string data into ascii: {}'.format(err)).serialize()
                    }
                    return json.dumps(error), files
                try:
                    file_content = base64.b64decode(file_content)
                except binascii.Error as err:
                    response = {
                        'timestamp': str(datetime.now()),
                        'success': False,
                        'data': None,
                        'error': BadRequest(
                            message='failed input data is not base64 data: {}'.format(err)).serialize()
                    }
                    return json.dumps(response), files
                file_content = item['content']

            files.append({'name':file_name, 'content':file_content})
    return error, files

def _outbox_add(sender,provider,to,subject,text,cc,bcc,html,user_json_variable,attachments,inline_images,reply_to):
    """
    Add email to outbox table
    
    return {
        'timestamp': 'string',                          Current timestamp (insert timestamp)
        'success': 'boolean',                           True message added to outbox; False otherwise
        'id': 'string',                                 GUID() string id of email
        'error': 'string'                               Error json data
    }
    """
    response={}
    
    # Select environment (production or development)
    environment = config.get('gemail/environment')
    
    # Get database gemail url
    connstr = config.get('gemail/%s/database_url' % environment)
    
    if connstr is None:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'error': NotFound(message='failed to obtain gemail database information').serialize()
        }
        return json.dumps(response)
    else:

        if "connect_timeout" not in connstr:
            connect_timeout = config.get('gemail/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout
                
        sql = "INSERT INTO public.outbox (created_at,updated_at,provider,recipients,subject,body,"
        sql = sql + "sender,cc,bcc,html,user_json_variable,attachments,inline_images,reply_to,status) "
        sql = sql + "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id;"
        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            dt = datetime.now()
            cur.execute(sql, (dt,dt,provider,to,subject,text,sender,cc,bcc,html,user_json_variable,attachments,inline_images,reply_to,'ready'))
            id_value = cur.fetchone()[0]
            conn.commit()
            cur.close()
            response = {
                'timestamp': str(datetime.now()),
                'success': True,
                'id': id_value,
            }
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'error': InternalServerError(message='Failed to update Outbox table. %s' % error).serialize()
            }
        finally:
            if conn is not None:
                conn.close()
    return json.dumps(response)

def _outbox_get_status(message_id):
    """
    Get email status from outbox

    return 
        {
            'timestamp': 'string'                       Current time
            'success': 'boolean',                       True if info is available; False otherwise
            'requested_id': 'string'                    The message id requested
            'data'   :   
                [
                    {
                        'id': 'string',                     GEmail Message Id
                        'provider_id': 'string',            Provider Message Id (could be null)
                        'user_json_variable': 'string',     GEmail user variables
                        'status': 'string',                 GEmail current status
                        'severity': 'string',               GEmail severity. The email provider can set the status to 
                                                                            failed o rejected permanent or temporary
                                                                            (could be null)
                        'provider_status': 'string',        Provider delivery status (could be null)
                        'error': 'string',                  Provider error message (could be null)
                        'created_at': 'string',             Timestamp when the email was added to the outbox
                        'updated_at':'string',              Last updated timestamp
                        'deleted_at':'string',              Deleted timestamp
                    }
                ]                               
                
            'error': 'string'                           Error if any (optional)
        }
    """
    response={}
    
    # Select environment (production or development)
    environment = config.get('gemail/environment')
    
    # Get database gemail url
    connstr = config.get('gemail/%s/database_url' % environment)
    
    if connstr is None:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'requested_id': message_id,  
            'error': NotFound(message='Get status failed to obtain gemail database information').serialize()
        }
        return json.dumps(response)
    else:

        if "connect_timeout" not in connstr:
            connect_timeout = config.get('gemail/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout

        sql = "select id,provider_id,user_json_variable,status,severity,provider_status,error,created_at,updated_at,deleted_at "
        sql = sql + "from public.outbox "
        sql = sql + "where id = '%s'" %  message_id
        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            cur.execute(sql)
            row = cur.fetchone()
            cur.close()
            if row:
                # Populate data
                data = {}
                idx = 0
                for name in ['id','provider_id','user_json_variable','status','severity','provider_status','error','insert_timestamp','updated_at','deleted_at']:
                    if isinstance(row[idx], datetime):
                        data[name] = str(row[idx])
                    else:
                        data[name] = row[idx]
                    idx+=1
                response = {
                    'timestamp': str(datetime.now()),
                    'success': True,
                    'requested_id': message_id,  
                    'data'   : [data],                                 
                }
            else:
                response = {
                    'timestamp': str(datetime.now()),
                    'success': False,
                    "requested_id": message_id,  
                    'error': NotFound(message='Get status failed. Email ID %s not found in outbox table' % message_id).serialize()
                }
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'requested_id': message_id,  
                'error': InternalServerError(message='Get status failed to retrieve Email ID %s from Outbox data. %s' % (message_id,error)).serialize()
            }
        finally:
            if conn is not None:
                conn.close()
    return json.dumps(response)

def _outbox_get_user_data_status(user_query):
    """
    Get email status from user data from outbox

    The format expected in the user variable query is:

        [{"field":"","operator":"","value":"", "logic_operator":"and"}, {"field":"","operator":"","value":"", logic_operator":"or"}, ...]
    
    Where:
        field           is any key from the original json provided in the user json variable
        operator        should be one of '=', '!='
        value           any data value 
        logic_operator  "and", "or"

    return 
        {
            'timestamp': 'string'                       Current time
            'success': 'boolean',                       True if info is available; False otherwise
            'requested_id': 'string'                    The user variable requested
            'data'   : [
                {
                    'id': 'string',                     GEmail Message Id
                    'provider_id': 'string',            Provider Message Id (could be null)
                    'user_json_variable': 'json',       GEmail user variables
                    'status': 'string',                 GEmail current status
                    'severity': 'string',               GEmail severity. The email provider can set the status to 
                                                         failed o rejected permanent or temporary (could be null)
                    'provider_status': 'string',        Provider delivery status (could be null)
                    'error': 'string',                  Provider error message (could be null)
                    'created_at': 'string',             Timestamp when the email was added to the outbox
                    'updated_at':'string',              Last updated timestamp
                    'deleted_at':'string',              Deleted timestamp
                },
                ...
            ],                                
            'error': 'string'                           Error if any (optional)
        }
    """
    response={}

    # Select environment (production or development)
    environment = config.get('gemail/environment')
    
    # Get database gemail url
    connstr = config.get('gemail/%s/database_url' % environment)
    
    if connstr is None:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'requested_id': user_query,  
            'error': NotFound(message='Get status failed to obtain gemail database information').serialize()
        }
        return json.dumps(response)
    else:

        if "connect_timeout" not in connstr:
            connect_timeout = config.get('gemail/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout

        user_data = user_query
        if user_data is None or isinstance(user_data, list) == False:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'requested_id': user_query,  
                'error': BadRequest(message='The user data test is not a valid json. Should be in this format [{"field":"","operator":"","value":"", "logic_operator":"and"}, ...]').serialize()
            }
            return json.dumps(response)

        #Create where statement
        where = ""
        count = len(user_data) 
        idx = 1
        error = None
        for data in user_data:
            if "field" in data:
                where= where + " (user_json_variable->>'" + data["field"] + "' "
            else:
                error = 'Missing field in query!'
                break
            
            if "operator" in data:
                where= where + data["operator"] + " "
            else:
                error = 'Missing operator in query!'
                break
            
            if "value" in data:
                where= where + "'" + data["value"] + "') "
            else:
                error = 'Missing value in query!'
                break

            if "logic_operator" in data:
                if idx < count:
                    where = where + data["logic_operator"] + " "
            elif idx < count:
                error = "Missing logical_operator in query!"
                break
            idx+=1

        if error != None:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'requested_id': user_query,  
                'error': BadRequest(message='Failed to parse the query due to: %s' % error).serialize()
            }
            return json.dumps(response)
       
        sql = "select id,provider_id,user_json_variable,status,severity,provider_status,error,created_at,updated_at,deleted_at "
        sql = sql + "from public.outbox "
        sql = sql + "where %s" % where
        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            cur.close()
            data = []
            if rows and len(rows)>0:
                for row in rows:
                    # Populate data
                    item = {}
                    idx = 0
                    for name in ['id','provider_id','user_json_variable','status','severity','provider_status','error',
                        'insert_timestamp','updated_at','deleted_at']:
                        if isinstance(row[idx], datetime):
                            item[name] = str(row[idx])
                        else:
                            item[name] = row[idx]
                        idx+=1
                    data.append(item)

                response = {
                    'timestamp': str(datetime.now()),
                    'success': True,
                    'requested_id': user_query,  
                    'data'   : data,                                 
                }
            else:
                response = {
                    'timestamp': str(datetime.now()),
                    'success': False,
                    "requested_id": user_query,  
                    'error': NotFound(message='Get status failed. User Variable %s not found in outbox table' % user_query).serialize()
                }
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'requested_id': user_query,  
                'error': InternalServerError(message='Get status failed to retrieve Email ID %s from Outbox data. %s' % (user_query,error)).serialize()
            }
        finally:
            if conn is not None:
                conn.close()
    return json.dumps(response)

def _search_emails(keyword):
    """
    Get list of emails that match the given keyword from the outbox

    return 
        {
            'timestamp': 'string'                       Current time
            'success': 'boolean',                       True if info is available; False otherwise
            'requested_id': 'string'                    The user variable requested
            'data'   : [
                {
                    'id': 'string',                     GEmail Message Id
                    'provider_id': 'string',            Provider Message Id (could be null)
                    'user_json_variable': 'json',       GEmail user variables
                    'status': 'string',                 GEmail current status
                    'severity': 'string',               GEmail severity. The email provider can set the status to 
                                                         failed o rejected permanent or temporary (could be null)
                    'provider_status': 'string',        Provider delivery status (could be null)
                    'error': 'string',                  Provider error message (could be null)                                                         
                    'created_at': 'string',             Timestamp when the email was added to the outbox
                    'updated_at':'string',              Last updated timestamp
                    'deleted_at':'string',              Deleted timestamp
                },
                ...
            ],                                
            'error': 'string'                           Error if any (optional)
        }
    """
    response={}

    # Select environment (production or development)
    environment = config.get('gemail/environment')
    
    # Get database gemail url
    connstr = config.get('gemail/%s/database_url' % environment)
    
    if connstr is None:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'error': NotFound(message='Get status failed to obtain gemail database information').serialize()
        }
        return json.dumps(response)
    else:

        if "connect_timeout" not in connstr:
            connect_timeout = config.get('gemail/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout
        
        sql = "select DISTINCT id,provider_id,user_json_variable,status,severity,provider_status,error,created_at,updated_at,deleted_at "
        sql = sql + " from ( "
        sql = sql + " select *,unnest(recipients) as n from public.outbox "
        sql = sql + " union "
        sql = sql + " select *,unnest(cc) as n from public.outbox "
        sql = sql + " union "
        sql = sql + " select *,unnest(bcc) as n from public.outbox "
        sql = sql + ") q where "
        sql = sql + " n like '%" + keyword + "%' "
        sql = sql + "or (subject ilike '%" + keyword + "%') "
        sql = sql + "or (body ilike '%" + keyword + "%') "
        sql = sql + "or (html ilike '%" + keyword + "%') "
        sql = sql + "or (attachments ilike '%" + keyword + "%') "
        sql = sql + "or (inline_images ilike '%" + keyword + "%') "
        sql = sql + "or (error ilike '%" + keyword + "%') "

        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            cur.close()
            data = []
            if rows and len(rows)>0:
                for row in rows:
                    # Populate data
                    item = {}
                    idx = 0
                    for name in ['id','provider_id','user_json_variable','status','severity','provider_status','error',
                        'insert_timestamp','updated_at','deleted_at']:
                        if isinstance(row[idx], datetime):
                            item[name] = str(row[idx])
                        else:
                            item[name] = row[idx]
                        idx+=1
                    data.append(item)

                response = {
                    'timestamp': str(datetime.now()),
                    'success': True,
                    'data'   : data,                                 
                }
            else:
                response = {
                    'timestamp': str(datetime.now()),
                    'success': False,
                    'error': NotFound(message='Search for keyword %s giving no results from in outbox table' % keyword).serialize()
                }
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'error': InternalServerError(message='Search command unhandled exception %s' % error).serialize()
            }
        finally:
            if conn is not None:
                conn.close()
    return json.dumps(response)

def _outbox_get_email(message_id):
    """
    Get email data from outbox

    return 
        {
            'timestamp': 'string'                       Current time
            'success': 'boolean',                       True if info is available; False otherwise
            'requested_id': 'string',                   The message id requested
            'data'   :  {
                'id': 'string',                         GEmail Message Id
                'provider_id': 'string',                Provider Message Id (could be null)
                'status': 'string',                     GEmail current status
                'provider_status': 'string',            Provider delivery status (could be null)
                'created_at': 'string',                 Timestamp when the email was added to the outbox
                'updated_at':'string',                  Last updated timestamp
                'deleted_at':'string',                  Deleted timestamp
                'provider': 'string',                   Provider name ('mailgun' or 'smtp')
                'to': 'list',                           Recipients list
                'subject':"string",                     Email subject
                'body':"string",                        Email body text
                'sender':"string",                      Sender "from" email
                'cc':"list",                            CC list
                'bcc':"list",                           BCC list
                'html':"string",                        Html text content
                'user_json_variable':"json",            Json of user variables
                'attachments':"list",                   List of attachments
                'inline_images':"list",                 List of inline_images
                'reply_to':'string'                     Default reply-to email address
            }
            'error': 'string'                           Error if any (optional)
        }
    return
    """
    response={}
    
    # Select environment (production or development)
    environment = config.get('gemail/environment')
    
    # Get database gemail url
    connstr = config.get('gemail/%s/database_url' % environment)

    if connstr is None:
        response = {
            'timestamp': str(datetime.now()),
            'success': False,
            'requested_id': message_id,  
            'error': NotFound(message='Get email failed to obtain gemail database information').serialize()
        }
        return json.dumps(response)
    else:
        if "connect_timeout" not in connstr:
            connect_timeout = config.get('gemail/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout

        sql = "select id,provider_id,status,provider_status,created_at,updated_at,deleted_at,provider,recipients,"
        sql = sql + "subject,body,sender,cc,bcc,html,user_json_variable,attachments,inline_images,reply_to from public.outbox "
        sql = sql + "where id = '%s'" % message_id
        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            cur.execute(sql)
            row = cur.fetchone()
            cur.close()
            if row:
                # Populate data
                data = {}
                idx = 0
                for name in ['id','provider_id','status','provider_status',
                    'created_at','updated_at','deleted_at','provider','to',
                    'subject','body','sender','cc','bcc','html','user_json_variable','attachments',
                    'inline_images','reply_to']:
                    if isinstance(row[idx], datetime):
                        data[name] = str(row[idx])
                    else:
                        data[name] = row[idx]
                    idx+=1
                response = {
                    'timestamp': str(datetime.now()),
                    'success': True,
                    'requested_id': message_id,  
                    'data'   : data,                                 
                }
            else:
                response = {
                    'timestamp': str(datetime.now()),
                    'success': False,
                    'requested_id': message_id,  
                    'error': NotFound(message='Get mail failed. Email ID %s not found in outbox table' % message_id).serialize()
                }
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            response = {
                'timestamp': str(datetime.now()),
                'success': False,
                'requested_id': message_id,  
                'error': InternalServerError(message='Get mail failed to retrieve Email ID %s from Outbox data. %s' % (message_id,error)).serialize()
            }
        finally:
            if conn is not None:
                conn.close()
    return json.dumps(response)
