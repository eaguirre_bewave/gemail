from wsgiref.simple_server import make_server

from spyne import Application
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication

from gemail.soap import generate_spyne_class


# This class should be used in development only.
# `CORS` should be configured in the web server for production.
class CORSWsgiApplication(WsgiApplication):
    """Enable serving of CORS requests.

    http://en.wikipedia.org/wiki/Cross-origin_resource_sharing
    """

    ALLOW_ORIGIN = '*'
    ALLOW_HEADERS = 'Origin, X-Requested-With, Content-Type, Authorization, SOAPAction'

    def __call__(self, req_env, start_response, wsgi_url=None):
        if req_env['REQUEST_METHOD'] == 'OPTIONS':
            start_response('204 No Content', [
                ('Access-Control-Allow-Origin', self.ALLOW_ORIGIN),
                ('Access-Control-Allow-Headers', self.ALLOW_HEADERS),
            ])
            return []

        return super(CORSWsgiApplication, self).__call__(req_env, start_response, wsgi_url)


soap_application = Application(
    [generate_spyne_class()],
    tns='garda.gsked.v1.soap',
    in_protocol=Soap11(),
    out_protocol=Soap11(),
)
wsgi_app = CORSWsgiApplication(soap_application)


if __name__ == '__main__':
    # You can use any Wsgi server. Here, we chose
    # Python's built-in wsgi server but you're not
    # supposed to use it in production.
    server = make_server('0.0.0.0', 8001, wsgi_app)
    server.serve_forever()
